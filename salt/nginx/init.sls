# Install a "vanilla" version of nginx from system packages

nginx:

  pkg.installed:
    - name: nginx

  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - require:
      - pkg: nginx
