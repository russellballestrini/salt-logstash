# Kibana web based interface for Logstash

# This state assumes that the /etc/nginx/sites-enabled/ directory is being 
# watched by the nginx service to restart

################## WARNING ############
# I totally remove default nginx page.
################## WARNING ############

# Include nginx for serving kibana
include:
  - nginx

# Where Kibana gets installed
kibana-home:
  file.directory:
    - name: /opt/kibana
    - user: www-data
    - group: www-data

# Get the kibana tar
kibana-tarball:
  file.managed: 
    - name: /opt/kibana/kibana-3.0.0milestone4.tar.gz
    - source: salt://kibana/kibana-3.0.0milestone4.tar.gz
    - require:
      - file.directory: /opt/kibana

# Untar it
kibana-untar:
  cmd.run:
    - name: tar zxvf /opt/kibana/kibana-3.0.0milestone4.tar.gz -C /opt/kibana/
    - unless: test -d /opt/kibana/kibana-3.0.0milestone4
    - require:
      - file: /opt/kibana/kibana-3.0.0milestone4.tar.gz

#install kibana config into nginx
kibana-nginx:
  file.managed:
    - name: /etc/nginx/sites-available/kibana
    - user: root
    - group: root
    - source: salt://kibana/config/kibana.nginx
    - require:
      - pkg: nginx

# Enable our kibana
kibana-nginx-enable:
  file.symlink:
    - name: /etc/nginx/sites-enabled/kibana
    - target: /etc/nginx/sites-available/kibana
    - require:
      - file: /etc/nginx/sites-available/kibana
      - pkg: nginx

# Kibana config file:
kibana-config:
  file.managed:
    - name: /opt/kibana/kibana-3.0.0milestone4/config.js
    - source: salt://kibana/config/config.js
    - user: www-data
    - group: www-data
    - template: jinja
    - require:
      - cmd.run: tar zxvf /opt/kibana/kibana-3.0.0milestone4.tar.gz -C /opt/kibana/
      - file.directory: /opt/kibana

# Remove nginx default
kibana-remove-nginx-default-page:
  file.absent:
    - name: /etc/nginx/sites-enabled/default

# Extend nginx service: restart nginx if certain kibana nginx confs change
extend:
  nginx:
    service:
      - watch: 
        - file.managed: kibana-nginx
        - file.managed: kibana-nginx-enable
        - file.absent: kibana-remove-nginx-default-page

