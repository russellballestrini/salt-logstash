# "Oracle" Java Installation
# Currently only interested in the Oracle 'Server' Java package.
# Server Jave has the following:
#   - No Auto Update
#   - JRE
#   - 64-bit only

# thanks to forrest for debugging help

java-home:
  file.directory:
    - name: /usr/java
    - user: root
    - group: root
    - mode: 755
# If subdirs don't exist, create it 
    - makedirs: True 

java-tarball:
  file.managed:
    - name: /usr/java/server-jre-{{ pillar['java-oracle']['server-jre']['version']}}-linux-x64.tar.gz
    - source: salt://java-oracle/server-jre-{{ pillar['java-oracle']['server-jre']['version']}}-linux-x64.tar.gz
    - user: root
    - group: root
    - mode: 755
    - require:
      - file.directory: /usr/java

# Untar 
java-latest-untar:
  cmd.run: 
    - name: tar xvf /usr/java/server-jre-{{ pillar['java-oracle']['server-jre']['version']}}-linux-x64.tar.gz -C /usr/java
    - unless: test -d /usr/java/latest/bin/
    - require:
      - file: java-tarball

# Symlink java to /usr/java/latest 
java-latest-symlink:
  cmd.run:
    - name: ln -sf /usr/java/{{pillar['java-oracle']['server-jre']['dir'] }} /usr/java/latest
    - unless:  test -L /usr/java/latest 
    - require:
      - cmd: java-latest-untar

# Install system wide 
java-oracle-sysinstall:
{% if grains['os'] == 'Ubuntu' %}
  alternatives.install:
    - name: java
    - link: /usr/bin/java
    - path: {{pillar['java-oracle']['server-jre']['path']}}/java
    - priority: 1000
    - require:
        # Prevent installing until symlink complete 
      - cmd: java-latest-symlink
{% endif %}
