{# 
    # prepared by bobby. 
    # tmux all the things
#}
tmux:
  pkg:
    - installed
{% if grains['os'] == 'Ubuntu' %}
    - name: tmux
{% elif grains['os'] == 'FreeBSD' %}
    - name: tmux-1.8
{% endif %}