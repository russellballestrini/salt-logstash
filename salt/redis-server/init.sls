# Redis-server
# Note: This installs the redis server, but does *NOT* configure it for anything

redis-server-daemon:
  pkg:
    - installed
    - name: redis-server

  service:
    - running
    - name: redis-server

