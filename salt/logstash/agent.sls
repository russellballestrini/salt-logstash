# Logstash agent is used to collect data from the configs specified in our drop directory.

# Logstash-agent-config-dir
logstash-agent-config-dir:
  file.recurse:
    - name: /etc/logstash/agent.d/
    - source: salt://logstash/agent.d


# Logstash agent upstart script (for services)
#   REQUIRES user: logstash
logstash-agent-service-conf:
  file.managed:
    - name: /etc/init/logstash-agent.conf
    - source: salt://logstash/service/logstash-agent.conf
    - template: jinja
    - user: root
    - group: root
    - require:
      - user: logstash

# Logstash daemon
logstash-agent-service:
  service:
    - name: logstash-agent
    - running
    - enable: True
    - reload: True
    - require:
      - file.managed: /etc/init/logstash-agent.conf
