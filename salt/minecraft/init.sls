{#
    # Future home of my minecraft server
#}

{# Minecraft server #}
minecraft-jar:
  file.managed:
    - name: {{ pillar['minecraft']['server']['path'] }}/minecraft_server.{{ pillar['minecraft']['server']['version'] }}.jar
    - source: salt://minecraft/minecraft_server.{{ pillar['minecraft']['server']['version'] }}.jar
    - owner: minecraft
    - group: minecraft
    - mode: 755
    - require:
      - user: minecraft

{# minecraft user - UID/GID left blank intentionally #}
minecraft-user:
  user.present:
    - name: {{ pillar['minecraft']['server']['user'] }}
    - fullname: Minecraft Server
    - shell: /bin/false
    - home: /opt/minecraft
    - groups:
      - minecraft
      - games
    - require:
      - group: minecraft

{# Minecraft Group #}
minecraft-group:
  group.present:
    - name: {{ pillar['minecraft']['server']['group'] }}

{# Minecraft upstart script #}
minecraft-upstart:
  file.managed:
    - name: /etc/init/minecraft.conf
    - user: root
    - group: root
    - mode: 644
    - source: salt://minecraft/minecraft.conf
    - template: jinja

{# Service #} 
minecraft-service:
  service:
    - name: minecraft
    - running
    - require:
      - file: minecraft-upstart
      