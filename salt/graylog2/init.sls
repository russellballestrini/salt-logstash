{% #This is going to be used for deploying Graylog2 someday %}
{% # Derived from https://github.com/hvnsweeting/saltstates/blob/master/graylog2/server.sls %}
graylog2:
  pkg:
    - installed
    - require:
      - file: graylog2-server-install
      - file: graylog2-web-interface-install

graylog2-server-install:
  file.managed:
    - name: /usr/local/src/graylog2-server-0.13.0-rc.1.tar.gz
    - source: salt://salt/graylog2/graylog2-server-0.13.0-rc.1.tar.gz

graylog2-web-interface-install:
  source: salt://salt/graylog2/graylog2-web-interface-0.12.0.tar.gz