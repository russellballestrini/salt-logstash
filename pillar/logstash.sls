{# The 'flatjar' package is the 'big' .jar file #} 
logstash:
  flatjar:
    path: /opt/logstash
    version: 1.3.2
    user: logstash
    group: logstash
    max_mem: 1024M
    min_mem: 512M
    cluster.name: logstash
