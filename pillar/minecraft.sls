minecraft:
  server:
    port: 25565
    md5: 39df9f29e6904ea7b351ffb4fe949881
    version: 1.6.2
    path: /opt/minecraft
    user: minecraft
    group: minecraft
    max_mem: 1024M
    min_mem: 1024M