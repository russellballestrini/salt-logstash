#!/bin/bash

case $1 in
        -h | --help)
                echo "usage: ./get_binaries.sh"
                echo ""
                echo "options:"
                echo "--force/-f        overwrite the existing binaries"
                echo "--remove/-r       remove the existing binaries"
                echo "" 
                ;;
        -f | --force)
                echo "Not implemented yet"
                ;;
        -r | --remove)
                echo "Remove existing binaries"
                /bin/rm -i salt/java-oracle/server-jre-7u45-linux-x64.tar.gz
                /bin/rm -i salt/logstash/logstash-1.3.2-flatjar.jar
                ;;
        # Without arguements just run the script
        *)

        # Get Java
        cd salt/java-oracle
        if [ ! -s ./server-jre-7u45-linux-x64.tar.gz ]
            then 
                echo "[*] Java JRE Not found, downloading now"
                wget http://bobbylikeslinux.net/dl/server-jre-7u45-linux-x64.tar.gz
                wget http://bobbylikeslinux.net/dl/server-jre-7u45-linux-x64.tar.gz.md5
        else
                # Display found + file size (human readable)
                echo "[+] Java JRE Found: `ls -lh ./server-jre-7u45-linux-x64.tar.gz| awk '{print $5}'`"
        fi
        
        # Get Logstash
        cd ../logstash
        if [ ! -s ./logstash-1.3.2-flatjar.jar ]
            then
                echo "[*] Logstash Not found, downloading now"
                wget https://download.elasticsearch.org/logstash/logstash/logstash-1.3.2-flatjar.jar
        else
                # Display found + file size (human readable)
                echo "[+] Logstash Flatjar Found:  `ls -lh ./logstash-1.3.2-flatjar.jar| awk '{print $5}'`"
        fi

        # Get Elasticsearch
        cd ../elasticsearch
        if [ ! -s ./elasticsearch-0.90.7.deb ]
            then 
                echo "[*] Elasticsearch Not found, downloading now"
                wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-0.90.7.deb
        else
                # Display found + file size (human readable)
                echo "[+] Elasticsearch .deb Found:  `ls -lh ./elasticsearch-0.90.7.deb| awk '{print $5}'`"
        fi
                
        # Get Kibana
        cd ../kibana
        if [ ! -s ./kibana-3.0.0milestone4.tar.gz  ]
            then 
                echo "[*] Kibana Not found, downloading now"
                wget https://download.elasticsearch.org/kibana/kibana/kibana-3.0.0milestone4.tar.gz 
        else
                # Display found + file size (human readable)
                echo "[+] Kibana .deb Found:  `ls -lh ./kibana-3.0.0milestone4.tar.gz | awk '{print $5}'`"
        fi
                ;;
esac
        
