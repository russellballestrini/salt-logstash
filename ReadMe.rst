robawt-salt
###########

Overview
========

This is just an experimental `Salt Stack <http://saltstack.org>`_ repo for
sandboxing. Everything within this repository is considered *public*.

Intent
======

The original intent of this `Salt Stack <http://saltstack.org>`_ repository is 
experimental, therefore do not consider anything worthy of production without 
analyzing the `Salt Stack <http://saltstack.org>`_ state/pillar yourself.


What Should The User Do?
========================

If this is the first time you've downloaded the repository:

#. Place this in a directory that your vagrant VM expects a root *Salt*
   filesystem
#. Make sure you run ``./get_binaries.sh`` in the root directory.



Feedback
========

comments/suggestions 
Contact the author ``robawt`` via bitbucket: https://bitbucket.org/robawt/

Currently Salted
================

* graylog2
    * CURRENT STATUS: States work with older version, have not tested with
      newest
    * Need to add binaries to ``Readme.rst``
* java-oracle
* Logstash
    * Redis-Server
    * ElasticSearch
    * Kibana + nginx
* minecraft
* tmux
    * some FreeBSD grain support
* ufw 
    * ssh allowed
    * salt allowed
* vim

URLs For Binaries
=================

For the use of this project, below are the URLs to the proper binaries

JAVA
----

``http://bobbylikeslinux.net/dl/server-jre-7u45-linux-x64.tar.gz.md5``
``http://bobbylikeslinux.net/dl/server-jre-7u45-linux-x64.tar.gz``

Logstash
--------

``https://download.elasticsearch.org/logstash/logstash/logstash-1.3.1-flatjar.jar``

Graylog2
--------

#TODO
